TIP: The Infinite Palette (JS and C programming languages)

Instead of storing many shades of each base color and never finding the exact
color we need, here is a function deriving the EXACT DESIRED variation of a
base color - using amounts of saturation and lightness:

- C programming language:
  unsigned int color = rgb_shade(0xff0000, 0.5, 0.1); // [0.0, 1.0]

- Javascript programming language:
  var color = rgb_shade(0xff0000, 0.5, 0.1); // [0.0, 1.0]

This C code is a translation of "color.js" that I wrote in 2021 for the 
SLIMalloc Web Console SaaS: http://twd.ag/archives/twd-slimalloc.html

Written by Pierre G. Gauthier, 2021-2023 (MIT license)

