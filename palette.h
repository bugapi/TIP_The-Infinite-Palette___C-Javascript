// =============================================================================
// TIP: The Infinite Palette (JS and C programming languages)
//
// Instead of storing many shades of each base color and never finding the exact
// color we need, here is a function deriving the EXACT DESIRED variation of a
// base color - using amounts of saturation and lightness:
//
// - C programming language:
//   unsigned int color = rgb_shade(0xff0000, 0.5, 0.1); // [0.0, 1.0]
//
// - Javascript programming language:
//   var color = color = rgb_shade(0xff0000, 0.5, 0.1); // [0.0, 1.0]
//
// This C code is a translation of "color.js" that I wrote in 2021 for the 
// SLIMalloc Web Console SaaS: http://twd.ag/archives/twd-slimalloc.html
//
// Written by Pierre G. Gauthier, 2021-2023 (MIT license)
// =============================================================================
#define PAL_RED          0xf44336
#define PAL_PINK         0xe91e63
#define PAL_PURPLE       0x9c27b0
#define PAL_VIOLET       0x673ab7
#define PAL_INDIGO       0x3f51b5
#define PAL_NAVY         0x3f80c0
#define PAL_BLUE         0x2196f3
#define PAL_SKY          0x03a9f4
#define PAL_CYAN         0x00bcd4
#define PAL_TEAL         0x009688
#define PAL_GREEN        0x4caf50
#define PAL_OLIVE        0x8bc34a
#define PAL_LIME         0xcddc39
#define PAL_YELLOW       0xffeb3b
#define PAL_AMBER        0xffc107
#define PAL_ORANGE       0xff9800
#define PAL_CARROT       0xff5722
#define PAL_BROWN        0x795548
#define PAL_WOOD         0xB89B72
#define PAL_GRAY         0x9e9e9e
#define PAL_DUSK         0x607d8b
#define PAL_CEMENT       0x7e8493
#define PAL_BLACK        0x0
#define PAL_WHITE        0xffffff

typedef struct { char *name; u32 c; } pal_t;
const pal_t s_pal[] = 
{ { "red",           PAL_RED },
  { "pink",          PAL_PINK },
  { "purple",        PAL_PURPLE },
  { "violet",        PAL_VIOLET },
  { "indigo",        PAL_INDIGO },
  { "navy",          PAL_NAVY },
  { "blue",          PAL_BLUE },
  { "sky",           PAL_SKY },
  { "cyan",          PAL_CYAN },
  { "teal",          PAL_TEAL },
  { "green",         PAL_GREEN },
  { "olive",         PAL_OLIVE },
  { "lime",          PAL_LIME },
  { "yellow",        PAL_YELLOW },
  { "amber",         PAL_AMBER },
  { "orange",        PAL_ORANGE },
  { "carrot",        PAL_CARROT },
  { "brown",         PAL_BROWN },
  { "wood",          PAL_WOOD },
  { "gray",          PAL_GRAY },
  { "dusk",          PAL_DUSK },
  { "cement",        PAL_CEMENT },
  { "black",         PAL_BLACK },
  { "white",         PAL_WHITE },
  { 0, 0 }
};

// -----------------------------------------------------------------------------
// convert u32 Color to RGB u8 triplet
// -----------------------------------------------------------------------------
#define u32_to_rgb(rgb, c) \
  do { rgb[0] = ((c) & 0xff0000) >> 16; \
       rgb[1] = ((c) & 0x00ff00) >>  8; \
       rgb[2] =  (c) & 0xff; \
  }while(0);

// -----------------------------------------------------------------------------
// convert RGB triplet to u32 Color
// -----------------------------------------------------------------------------
#define rgb_to_u32(r,g,b)   (((u32)(r) << 16) | ((u32)(g) << 8) | (b))

// -----------------------------------------------------------------------------
// just in case you wonder how to display these values...
// -----------------------------------------------------------------------------
static char *hsl2str(char *str, double h, double s, double l)
{ 
  sprintf(str, "hsl(%u, %u%%, %u%%)", // H[0-360], SL[0-1]
         (u32)h, (u32)((s * 100) + 0.5f), (u32)((l * 100) + 0.5f));
  return str;
}
// -----------------------------------------------------------------------------
static char *rgb2hex(char *str, u8 *rgb) // u8 rgb[] to string
{ 
  sprintf(str, "0x%02X%02X%02X", rgb[0], rgb[1], rgb[2]);
  return str;
}
// -----------------------------------------------------------------------------
static char *bin2hex(char *str, u32 bin) // u32 color to hexadecimal string
{ u8 rgb[3];
  u32_to_rgb(rgb, bin); 
  return rgb2hex(str, rgb);
}

// -----------------------------------------------------------------------------
// calculate the lightness (luminance) value of an RGB triplet:
// L = (max(R, G, B) + min(R, G, B)) / 2;
// -----------------------------------------------------------------------------
static u8 rgb2luma(int red, int green, int blue) // [0-255]
{
  int min, max;

  if(red > green) max = max(red,   blue), min = min(green, blue);
  else            max = max(green, blue), min = min(red,   blue);

  #define round(x) ( (int)((x) + 0.5f) ) // much faster than libmath round()
  return round((max + min) / 2.0); // [0, 255]
  #undef round
}
// -----------------------------------------------------------------------------
// convert RGB color to HSL
// -----------------------------------------------------------------------------
static void rgb2hsl(double *hsl, u8 *_rgb) // H:[float:0-360] SL:[0-100]
{ 
  double rgb[3] = { _rgb[0] / 255., _rgb[1] / 255., _rgb[2] / 255. };
  double min = rgb[0], max = rgb[0], l, s, h;
  u8 vmax; vmax = 0;

  if(rgb[1] <= min) min = rgb[1]; if(rgb[1] >= max) max = rgb[1], vmax = 1;
  if(rgb[2] <= min) min = rgb[2]; if(rgb[2] >= max) max = rgb[2], vmax = 2;
//double d = max - min; // using D then creates more losses of precision

  if(vmax == 0) h = (rgb[1] - rgb[2]) / (max - min);     else
  if(vmax == 1) h = (rgb[2] - rgb[0]) / (max - min) + 2; else
  if(vmax == 2) h = (rgb[0] - rgb[1]) / (max - min) + 4;
  if(isNaNd(h)) h = 0;

  h *= 60; if(h < 0) h += 360;
  l = (min + max) / 2;
  if(min == max) s = 0;
  else 
     s = (l < 0.5f) ? (max - min) / (max + min)
                    : (max - min) / (2 - max - min);
 
  hsl[0] = h, hsl[1] = s, hsl[2] = l;
}
// -----------------------------------------------------------------------------
#define hsl_to_val(t1, t2, hue) \
  ({ double h = hue, m1 = t1, m2 = t2, res; \
     if(h <  0) h += 6; \
     if(h >= 6) h -= 6; \
     if(h <  1) res = (m2 - m1) * h + m1; else \
     if(h <  3) res = m2;                 else \
     if(h <  4) res = (m2 - m1) * (4 - h) + m1; \
     else       res = m1; \
     res; \
  })

static void hsl2rgb(u8 *rgb, double *hsl)
{ 
  double hue = hsl[0] / 60, sat = hsl[1], light = hsl[2];
  double t1, t2;
  if(light <= .5) t2 = light * (sat + 1);
  else            t2 = light + sat - (light * sat);
  t1 = light * 2 - t2;
  rgb[0] = (u8)(hsl_to_val(t1, t2, hue + 2) * 255);
  rgb[1] = (u8)(hsl_to_val(t1, t2, hue)     * 255);
  rgb[2] = (u8)(hsl_to_val(t1, t2, hue - 2) * 255);
}
// -----------------------------------------------------------------------------
// set the saturation and/or the brightness of a given u32 RGB color
// -----------------------------------------------------------------------------
// u32 color = rgb_shade(PAL_RED,  -1, .1);
// u32 color = rgb_shade(0xff0000, -1, .1);
// -----------------------------------------------------------------------------
static u32 rgb_shade(u32 dec, double sat, double bri)
{ 
  double hsl[3];
  u8 rgb[3];
  u32_to_rgb(rgb, dec); 
  rgb2hsl(hsl, rgb);
  if(sat > 0) hsl[1] = sat; // keep hue, modify saturation or brightness > 0
  if(bri > 0) hsl[2] = bri;
  if(isNaNd(hsl[1])) hsl[1] = 0;
  if(isNaNd(hsl[2])) hsl[2] = 0;
  hsl2rgb(rgb, hsl);
  return rgb_to_u32(rgb[0], rgb[1], rgb[2]);
}
// -----------------------------------------------------------------------------
static void hsl_shade(double *hsl, u32 dec, double sat, double bri)
{
  u8 rgb[3];
  u32_to_rgb(rgb, dec); 
  rgb2hsl(hsl, rgb);
  if(sat > 0) hsl[1] = sat; // keep hue, modify saturation or brightness > 0
  if(bri > 0) hsl[2] = bri;
  if(isNaNd(hsl[1])) hsl[1] = 0;
  if(isNaNd(hsl[2])) hsl[2] = 0;
}
// ============================================================================
// End of Source Code
// ============================================================================

