// =============================================================================
// TIP: The Infinite Palette (JS, C)
//
// Instead of storing many shades of each base color and never finding the exact
// color we need, here is a function deriving the EXACT DESIRED variation of a
// base color - using amounts of saturation and lightness in the [-1, +1] and
// [0, 1] ranges:
//
// - C programming language:
//    //                    color,    saturation, lightness
//    var color = rgb_shade(0xff0000, 0.5,        0.1); // [0.0, 1.0]
//
// - Javascript programming language:
//   var color = rgbShade(0xff0000,   50,          10); // [0, 100%]
//
// This C code is a translation of "color.js" that I wrote in 2021 for the 
// SLIMalloc Web Console SaaS: http://twd.ag/archives/twd-slimalloc.html
//
// Written by Pierre G. Gauthier, 2023 (MIT license)
// =============================================================================

// -----------------------------------------------------------------------------
// Base "Material Colors" Palette (JSON array)
// -----------------------------------------------------------------------------
const pal = { // JS: var color = pal.red; // --color-xxx are needed for CSS
 "red":0xf44336,  "pink":0xe91e63, "purple":0x9c27b0, "deep_purple":0x673ab7,
 "indigo":0x3f51b5, "navy":0x3f80c0, "blue":0x2196f3, "light_blue":0x03a9f4,
 "cyan":0x00bcd4, "teal":0x009688, "green":0x4caf50, "light_green":0x8bc34a,
 "lime":0xcddc39, "yellow":0xffeb3b, "amber":0xffc107, "orange":0xff9800,
 "deep_orange":0xff5722, "brown":0x795548, "grey":0x9e9e9e, "blue_grey":0x607d8b,
 "purple_grey":0x7e8493, "black":0, "white":0xffffff
};

const palstr = { // JS: var color = palpalstr.red; // --color-xxx are needed for CSS
 "red":"#f44336",  "pink":"#e91e63", "purple":"#9c27b0", "deep_purple":"#673ab7",
 "indigo":"#3f51b5", "navy":"#3f80c0", "blue":"#2196f3", "light_blue":"#03a9f4",
 "cyan":"#00bcd4", "teal":"#009688", "green":"#4caf50", "light_green":"#8bc34a",
 "lime":"#cddc39", "yellow":"#ffeb3b", "amber":"#ffc107", "orange":"#ff9800",
 "deep_orange":"#ff5722", "brown":"#795548", "grey":"#9e9e9e", "blue_grey":"#607d8b",
 "purple_grey":"#7e8493", "black":"#000", "white":"#fff"
};
// -----------------------------------------------------------------------------
// convert DEC color to RGB
// -----------------------------------------------------------------------------
function dec2rgb(d)
{ var rgb = [];
  rgb[0] = (d & 0xff0000) >>> 16;
  rgb[1] = (d & 0x00ff00) >>> 8;
  rgb[2] = d & 0xff;
  return rgb;
}
// -----------------------------------------------------------------------------
// convert RGB color to DEC
// -----------------------------------------------------------------------------
function rgb2dec(r, g, b){return ((r << 16) | (g << 8) | b)}
// -----------------------------------------------------------------------------
function hue2rgb(t1, t2, hue)
{ if(hue < 0) hue += 6;
  if(hue >= 6) hue -= 6;
  if(hue < 1) return (t2 - t1) * hue + t1;
  else if(hue < 3) return t2;
  else if(hue < 4) return (t2 - t1) * (4 - hue) + t1;
  else return t1;
}
// -----------------------------------------------------------------------------
// convert RGB color to HSL
// -----------------------------------------------------------------------------
function rgb2hsl(r, g, b) // H:[float:0-360] S & L:["0%"-"100%"]
{ var min, max, i, l, s, maxcol, h, rgb = [], hsl = [];
  rgb[0] = r / 255; rgb[1] = g / 255; rgb[2] = b / 255;
  min = rgb[0]; max = rgb[0];
  maxcol = 0;
  for(i = 0; i < rgb.length - 1; i++)
  { if(rgb[i + 1] <= min){min = rgb[i + 1]}
    if(rgb[i + 1] >= max){max = rgb[i + 1];maxcol = i + 1}
  }
  if(maxcol == 0){h = (rgb[1] - rgb[2]) / (max - min)} else
  if(maxcol == 1){h = 2 + (rgb[2] - rgb[0]) / (max - min)} else
  if(maxcol == 2){h = 4 + (rgb[0] - rgb[1]) / (max - min)}
  if(isNaN(h)) h = 0;
  h *= 60;
  if(h < 0) h += 360;
  l = (min + max) / 2;
  if(min == max) s = 0;
  else 
  { if(l < 0.5){s = (max - min) / (max + min)} 
    else {s = (max - min) / (2 - max - min)}
  }
  //s = s;
  hsl[0] = h, hsl[1] = s, hsl[2] = l;
  return hsl;
}
// -----------------------------------------------------------------------------
function hsl2rgb(hue, sat, light)
{ var t1, t2, rgb = [];
  hue = hue / 60;
  if(light <= .5){t2 = light * (sat + 1)} 
  else {t2 = light + sat - (light * sat)}
  t1 = light * 2 - t2;
  rgb[0] = hue2rgb(t1, t2, hue + 2) * 255;
  rgb[1] = hue2rgb(t1, t2, hue) * 255;
  rgb[2] = hue2rgb(t1, t2, hue - 2) * 255;
  return rgb;
}
// -----------------------------------------------------------------------------
// set the saturation and/or the brightness of a given DEC color
// -----------------------------------------------------------------------------
// var color = rgbShade(var(--color-red), -1, .1); // CAN'T WORK: var() for CSS
// var color = rgbShade(pal.red,  -1, .1); // JSON object   (integer color)
// var color = rgbShade(0xff0000, -1, .1); // integer color (hexadecimal format)
// var color = rgbShade(red,      -1, .1); // HTML integer color
// -----------------------------------------------------------------------------
function rgbShade(dec, sat, bri)
{ var hsl = [], rgb = [];
  rgb = dec2rgb(dec); 
  hsl = rgb2hsl(rgb[0], rgb[1], rgb[2]);
  if(sat > 0) hsl[1] = sat; // keep hue, modify saturation or brightness > 0
  if(bri > 0) hsl[2] = bri;
  if(isNaN(hsl[1])) hsl[1] = 0;
  if(isNaN(hsl[2])) hsl[2] = 0;
  rgb = hsl2rgb(hsl[0], hsl[1], hsl[2]);
  dec = rgb2dec(rgb[0], rgb[1], rgb[2]);
  return dec;
}
// -----------------------------------------------------------------------------
// decShade with HSL string output (needed when 0xff0000 will be stringified)
// -----------------------------------------------------------------------------
function shadeHsl(dec, sat, bri)
{ var hsl = [], rgb = [];
  rgb = dec2rgb(dec); 
  hsl = rgb2hsl(rgb[0], rgb[1], rgb[2]);
  if(sat > 0) hsl[1] = sat; // keep hue, modify saturation or brightness > 0
  if(bri > 0) hsl[2] = bri;
  if(isNaN(hsl[1])) hsl[1] = 0;
  if(isNaN(hsl[2])) hsl[2] = 0;
  return hsl2str(hsl[0],hsl[1],hsl[2]);
}
/* -----------------------------------------------------------------------------
// decShade with RGB string output (slower than above)
function shadeRgb(dec, sat, bri)
{ var hsl = [], rgb = [];
  rgb = dec2rgb(dec); 
  hsl = rgb2hsl(rgb[0], rgb[1], rgb[2]);
  if(sat > 0) hsl[1] = sat; // keep hue, modify saturation or brightness > 0
  if(bri > 0) hsl[2] = bri;
  if(isNaN(hsl[1])) hsl[1] = 0;
  if(isNaN(hsl[2])) hsl[2] = 0;
  rgb = hsl2rgb(hsl[0], hsl[1], hsl[2]);
  return rgb2str(rgb[0],rgb[1],rgb[2]);
}*/
// -----------------------------------------------------------------------------
function rgb2str(r, g, b)
{ return "rgb("+ r +","+ g +","+ b +")"; }
// -----------------------------------------------------------------------------
function hsl2str(h, s, l)
{ return "hsl("+ h +","+ Math.round(s*100) +"%,"+ Math.round(l*100) +"%)";}
// -----------------------------------------------------------------------------
function toHex(n)
{ var hex = n.toString(16);
  while(hex.length < 2) hex = "0" + hex;
  return hex;
}
// -----------------------------------------------------------------------------
function rgb2hex(r, g, b)
{ return "#" +  toHex(r) + toHex(g) + toHex(b);}
// -----------------------------------------------------------------------------
function dec2hex(dec)
{ rgb = [];
  rgb = dec2rgb(dec);
  return "#" +  toHex(rgb[0]) + toHex(rgb[1]) + toHex(rgb[2]);}
// -----------------------------------------------------------------------------
// DOES NOT WORK... (empty string)
// -----------------------------------------------------------------------------
function get_var(val)
{ var r = document.querySelector(':root, [data-theme="default"]');  
//var r = document.querySelector(':root, [data-theme="dark"]');  
//var r = document.querySelector(':root');  
  var rs = getComputedStyle(r); // get root styles (properties and values)
  return rs.getPropertyValue(val);
}
// -----------------------------------------------------------------------------

